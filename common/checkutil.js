

module.exports = {
    checkForField: function(req, name) {
        return req.body[name] !== undefined && req.body[name].length > 0
    }
}